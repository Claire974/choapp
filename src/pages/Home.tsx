import {
  IonContent, IonHeader, IonPage, IonTitle, IonToolbar,
  IonItem, IonCheckbox, IonLabel, IonNote, IonBadge, IonButton
 } from '@ionic/react';
import './Home.css';
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition';

const Home: React.FC = () => {
  let { transcript, resetTranscript } = useSpeechRecognition();
  const str_start = '<IonItem><IonCheckbox slot="start" /><IonLabel><h1>';
  const str_end = '</h1></IonLabel><IonBadge color="success" slot="end">sup</IonBadge></IonItem>';
  let transcript_tab = transcript.split("prout ");
  console.log(transcript_tab);
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>CHO APP</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonButton color="primary" onClick={ () => startReco() }>start</IonButton>
        <IonButton color="primary" onClick={ () => stopReco() }>stop</IonButton>
        <IonButton color="primary" onClick={ resetTranscript }>delete</IonButton>
        {/* <pre>{transcript.replaceAll("prout ", "\n")}</pre> */}
        {transcript_tab.map((product) =>
            <IonItem><IonCheckbox slot="start" /><IonLabel><h1>{product}</h1></IonLabel><IonBadge color="success" slot="end">sup</IonBadge></IonItem>
        )}
        {/* {
          transcript_tab.forEach((el) => {
              console.log(el);
                <IonItem><IonCheckbox slot="start" /><IonLabel><h1>{el}</h1></IonLabel><IonBadge color="success" slot="end">sup</IonBadge></IonItem>
          })
        } */}
      </IonContent>
    </IonPage>
  );
};

function startReco() {
  SpeechRecognition.startListening({
    continuous: true,
    language: 'fr-FR'
  });
}
function stopReco() {
  SpeechRecognition.stopListening();
}

export default Home;